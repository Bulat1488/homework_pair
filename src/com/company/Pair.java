package com.company;

import java.util.Objects;

class Pair <K, V> {

    private K first;
    private V second;

    private Pair(K f, V s) {
        this.first = f;
        this.second = s;
    }



    public static <K, V> Pair<K, V> of(K f, V s) {
        return new Pair(f, s);
    }

    public K getFirst() {
        return first;
    }

    public V getSecond() {
        return second;
    }
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (Pair.class.isInstance(other)) {
            return Objects.equals(first, ((Pair<?, ?>) other).first) &&
                    Objects.equals(second, ((Pair<?, ?>) other).second);
        }
        return false;
    }

    public int hashCode() {
        return Objects.hashCode(this.getFirst()) ^ Objects.hashCode(this.getSecond());
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}

